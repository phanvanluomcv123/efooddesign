package com.example.efood

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.ImageView

class Review : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)

        val btnBackReview = findViewById<ImageView>(R.id.imgViewBackReview)
        btnBackReview.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra("REVIEW_RETURN_TO_HOME", true)
            startActivity(intent)
        }
    }
}