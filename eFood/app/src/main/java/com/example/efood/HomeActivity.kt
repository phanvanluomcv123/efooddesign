package com.example.efood

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.ImageView

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val btnBack = findViewById<ImageView>(R.id.imgBtnMoreReview)
        btnBack.setOnClickListener {
            val intent = Intent(this, Review::class.java)
           intent.putExtra("HOME_SEND_REVIEW", true);
           startActivity(intent)
       }
    }
}